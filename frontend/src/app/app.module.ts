import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http'

import { AppRoutingModule }      from './app-routing.module';
import { AppComponent }          from './app.component';
import { LoginComponent}         from './components/login/login.component';
import { AddTheseComponent }     from './components/add-these/add-these.component';
import { TheseDetailsComponent } from './components/these-details/these-details.component';
import { ThesesListComponent }   from './components/theses-list/theses-list.component';
import { RegisterComponent }     from './components/register/register.component';
import { HomeComponent }         from './components/home/home.component';
import { ProfileComponent }      from './components/profile/profile.component';
import { BoardAdminComponent }   from './components/board-admin/board-admin.component';
import { BoardModeratorComponent}from './components/board-moderator/board-moderator.component';
import { BoardUserComponent }    from './components/board-user/board-user.component';

import {authInterceptorProviders}from './helpers/auth.interceptor';
import { RecuperationThesesSiteOfficielComponent } from './components/recuperation-theses-site-officiel/recuperation-theses-site-officiel.component';
import { TwitterComponent } from './components/twitter/twitter.component';
import { RssReaderComponent } from './components/rss-reader/rss-reader.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

@NgModule({
  declarations: [
    AppComponent,
    AddTheseComponent,
    TheseDetailsComponent,
    ThesesListComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    ProfileComponent,
    BoardAdminComponent,
    BoardModeratorComponent,
    BoardUserComponent,
    RecuperationThesesSiteOfficielComponent,
    TwitterComponent,
    RssReaderComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    Ng2SearchPipeModule
  ],
  providers: [authInterceptorProviders],
  bootstrap: [AppComponent]
})
export class AppModule { }
