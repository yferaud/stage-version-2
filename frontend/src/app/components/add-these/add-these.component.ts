import { Component, OnInit } from '@angular/core';
import { These } from 'src/app/models/these.model';
import { TheseService } from 'src/app/services/these.service';


@Component({
  selector: 'app-add-these',
  templateUrl: './add-these.component.html',
  styleUrls: ['./add-these.component.css']
})
export class AddTheseComponent implements OnInit {
 
  these: These = {
        dateInsert: undefined,
        dateMaj: undefined,
        status: '',
        accessible: undefined,
        titre: '',
        auteurPpn: 1,
        auteur: '',
        etabSoutenance: '',
        etabSoutenancePpn: undefined, 
        dateSoutenance: undefined , 
        discipline: '',
        num: '', 
        langueThese: [], 
        personne: [],
        ppn: [],
        oaiSetSpec: [],
        directeurThesePpn: [],
        directeurTheseNP: [],
        directeurThese: [],
        etablissement: [],
  };
  submitted = false;

  constructor(private theseService: TheseService) { }

  ngOnInit(): void {
  }

  saveThese(): void {
    const data= {
      dateInsert: this.these.dateInsert ,
      dateMaj: this.these.dateMaj,
      status: this.these.status,
      accessible: this.these.accessible,
      titre: this.these.titre,
      auteurPpn: this.these.auteurPpn,
      auteur: this.these.auteur,
      etabSoutenance: this.these.etabSoutenance,
      etabSoutenancePpn: this.these.etabSoutenancePpn, 
      dateSoutenance: this.these.dateSoutenance , 
      discipline: this.these.discipline,
      num: this.these.num, 
      langueThese: this.these.langueThese, 
      personne: this.these.personne,
      ppn: this.these.ppn,
      oaiSetSpec: this.these.oaiSetSpec,
      directeurThesePpn:this.these.directeurThesePpn,
      directeurTheseNP: this.these.directeurTheseNP,
      directeurThese: this.these.directeurThese,
      etablissement: this.these.etablissement,
    };

    this.theseService.create(data)
        .subscribe(
          response => {
            console.log(response);
          this.submitted =true;
          },
          error => {
            console.log(error);
        
          }
        );
    
  }

  newThese():void {
    this.submitted=false;
    this.these= {
      titre:'',
      auteur:'',
      langueThese:[]
    }
  }

}
