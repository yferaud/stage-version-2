import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddTheseComponent } from './add-these.component';

describe('AddTheseComponent', () => {
  let component: AddTheseComponent;
  let fixture: ComponentFixture<AddTheseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddTheseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddTheseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
