import { Component, OnInit } from '@angular/core';
import { These } from 'src/app/models/these.model';
import { TheseService } from 'src/app/services/these.service';
import { HttpClient } from '@angular/common/http';
import { TokenStorageService } from 'src/app/services/token-storage.service';

@Component({
  selector: 'app-theses-list',
  templateUrl: './theses-list.component.html',
  styleUrls: ['./theses-list.component.css']
})
export class ThesesListComponent implements OnInit {

  theses?:These[];
  currentThese: These= {};
  currentIndex = -1;
  title='';
  
  isLoggedIn = false;
  private roles: string[] = [];



  constructor( private theseService: TheseService,
               private http: HttpClient,
               private tokenStorageService: TokenStorageService ) { }
  
  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenStorageService.getToken();

    if (this.isLoggedIn) {
      const user = this.tokenStorageService.getUser();
      this.roles = user.roles;
    }

    this.retrieveTheses;
    this.searchTitle;
  }



  //permet d'afficher la liste
  retrieveTheses(): void {
    this.theseService.getAll()
    .subscribe (
      data =>{
        this.theses = data;
        console.log(data);
      },
      error => {
        console.log(error);
      });
    }

    //permet de refresh la liste
    refreshList(): void {
      this.retrieveTheses();
      this.currentThese = {};
      this.currentIndex = -1;
    }

    //choisir la "thèse active", celle que l'on a sélectionné.
    setActiveThese (these:These, index:number): void {
      this.currentThese=these;
      this.currentIndex=index;
    }

    //supprimer toutes les thèses et réactualiser la liste
    removeAllTheses(): void{
      this.theseService.deleteAll()
      .subscribe(
        response=> {
          console.log(response);
          this.refreshList();
        },
        error => {
          console.log(error);
        }
      );
    }



    //rechercher par titre de la thèse
    searchTitle(): void {
      this.currentThese= {};
      this.currentIndex=-1;

      this.theseService.findByTitle(this.title)
          .subscribe(
            data=> {
              this.theses=data;
              console.log(data);
            },
            error => {
              console.log(error);
            }
          );  
    }
}
function http(http: any) {
  throw new Error('Function not implemented.');
}

