import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { These } from 'src/app/models/these.model';
import { TheseService } from 'src/app/services/these.service';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service'; 
import { TokenStorageService } from 'src/app/services/token-storage.service';

import { FormsModule } from '@angular/forms';
import { ListTheseFromWebsite } from 'src/app/models/listTheseFromWebsite.model';
import { pipe } from 'rxjs';
import { map } from 'rxjs/operators';
import { Response } from 'src/app/models/response.model';


@Component({
  selector: 'app-recuperation-theses-site-officiel',
  templateUrl: './recuperation-theses-site-officiel.component.html',
  styleUrls: ['./recuperation-theses-site-officiel.component.css']
})
export class RecuperationThesesSiteOfficielComponent implements OnInit {
  
  listThesesFromWebsite: any=[];
  sujetRecherche='';
  these: These = {
    dateInsert: undefined,
    dateMaj: undefined,
    status: '',
    accessible: undefined,
    titre: '',
    auteurPpn: 1,
    auteur: '',
    etabSoutenance: '',
    etabSoutenancePpn: undefined, 
    dateSoutenance: undefined , 
    href:'',
    discipline: '',
    num: '', 
    langueThese: [], 
    personne: [],
    ppn: [],
    oaiSetSpec: [],
    directeurThesePpn: [],
    directeurTheseNP: [],
    directeurThese: [],
    etablissement: [],
};
submitted = false;
currentThese: These ={
  dateInsert: undefined,
    dateMaj: undefined,
    status: '',
    accessible: undefined,
    href:'',
    titre: '',
    auteurPpn: 1,
    auteur: '',
    etabSoutenance: '',
    etabSoutenancePpn: undefined, 
    dateSoutenance: undefined , 
    discipline: '',
    num: '', 
    langueThese: [], 
    personne: [],
    ppn: [],
    oaiSetSpec: [],
    directeurThesePpn: [],
    directeurTheseNP: [],
    directeurThese: [],
    etablissement: [],
  
}
message ='';
currentIndex = -1;
isLoggedIn = false;
private roles: string[] = [];



constructor(private http: HttpClient,
            private theseService:TheseService,
            private route: ActivatedRoute,
            private tokenStorageService: TokenStorageService
           /* private router: Router */
           ) {}

ngOnInit(): void {
  
  this.isLoggedIn = !!this.tokenStorageService.getToken();

  if (this.isLoggedIn) {
    const user = this.tokenStorageService.getUser();
    this.roles = user.roles;}
  this.message ='';
 /* this.getThese(this.route.snapshot.params.id); */
}

displayData(): void {
    const urlThesesSite= "http://www.theses.fr/?q="+this.sujetRecherche+"&fq=dateSoutenance:[1965-01-01T23:59:59Z%2BTO%2B2031-12-31T23:59:59Z]&checkedfacets=discipline=Informatique;&start=0&status=&access=&prevision=&filtrepersonne=&zone1=titreRAs&val1=&op1=AND&zone2=auteurs&val2=&op2=AND&zone3=etabSoutenances&val3=&op3=AND&zone4=dateSoutenance&val4a=&val4b=&type=&format=json"
    this.http.get</*any*/[]>(urlThesesSite)
    .subscribe(
      (data)=>{
        this.listThesesFromWebsite = data;
        console.warn(typeof(this.listThesesFromWebsite));
        console.log(this.listThesesFromWebsite);   
        },    
    error => {
      console.log(error);
    });
    
    
}

setActiveThese (these:These, index:number): void {
  this.currentThese=these;
  this.currentIndex=index;
}


getThese(id: string): void {
  this.theseService.get(id)
  .subscribe(
    data => {
      this.currentThese =data;
      console.log(data);
    },
    error => {
      console.log(error);
    }
  );
}
 

saveThese(): void {
    const data= {
     
      dateInsert: this.currentThese.dateInsert ,
      dateMaj: this.currentThese.dateMaj,
      status: this.currentThese.status,
      accessible: this.currentThese.accessible,
      titre: this.currentThese.titre,
      auteurPpn: this.currentThese.auteurPpn,
      auteur: this.currentThese.auteur,
      etabSoutenance: this.currentThese.etabSoutenance,
      etabSoutenancePpn: this.currentThese.etabSoutenancePpn, 
      dateSoutenance: this.currentThese.dateSoutenance , 
      discipline: this.currentThese.discipline,
      num: this.currentThese.num, 
      languecurrentThese: this.currentThese.langueThese, 
      personne: this.currentThese.personne,
      ppn: this.currentThese.ppn,
      oaiSetSpec: this.currentThese.oaiSetSpec,
      directeurcurrentThesePpn:this.currentThese.directeurThesePpn,
      directeurcurrentTheseNP: this.currentThese.directeurTheseNP,
      directeurcurrentThese: this.currentThese.directeurThese,
      etablissement: this.currentThese.etablissement,
    };

    this.theseService.create(data)
        .subscribe(
          response => {
            console.log(response);
          this.submitted =true;
          },
          error => {
            console.log(error);
        
          }
        );
}
 newThese():void {
    this.submitted=false;
    this.these= {
      titre:'',
      auteur:'',
      langueThese:[]
    }
  }

}