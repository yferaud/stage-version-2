import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecuperationThesesSiteOfficielComponent } from './recuperation-theses-site-officiel.component';

describe('RecuperationThesesSiteOfficielComponent', () => {
  let component: RecuperationThesesSiteOfficielComponent;
  let fixture: ComponentFixture<RecuperationThesesSiteOfficielComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecuperationThesesSiteOfficielComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecuperationThesesSiteOfficielComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
