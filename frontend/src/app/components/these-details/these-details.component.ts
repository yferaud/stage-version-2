import { Component, OnInit } from '@angular/core';
import { TheseService } from 'src/app/services/these.service';
import { ActivatedRoute, Router } from '@angular/router';
import { These } from 'src/app/models/these.model';
import { TokenStorageService } from 'src/app/services/token-storage.service';

@Component({
  selector: 'app-these-details',
  templateUrl: './these-details.component.html',
  styleUrls: ['./these-details.component.css']
})
export class TheseDetailsComponent implements OnInit {
 
  currentThese: These ={
    titre: '',
    langueThese: [],
    
  }
  isLoggedIn = false;
  private roles: string[] = [];
  message ='';

  constructor(
    private theseService: TheseService,
    private route: ActivatedRoute,
    private router: Router,
    private tokenStorageService: TokenStorageService) { }

  ngOnInit(): void {
    this.message ='';
    this.getThese(this.route.snapshot.params.id);
  }

  getThese(id: string): void {
    this.isLoggedIn = !!this.tokenStorageService.getToken();

    if (this.isLoggedIn) {
      const user = this.tokenStorageService.getUser();
      this.roles = user.roles;}
      
    this.theseService.get(id)
    .subscribe(
      data => {
        this.currentThese =data;
        console.log(data);
      },
      error => {
        console.log(error);
      }
    );
  }

  updateThese(): void {
    this.message='';

    this.theseService.update(this.currentThese.id, this.currentThese)
        .subscribe(
          response => {
            console.log(response);
            this.message= response.message ? response.message :'This These was updated'
          },
          error => {
            console.log(error);
          }
        );
      
  }

  deleteThese(): void {
    this.theseService.delete(this.currentThese.id)
    .subscribe(
      response => {
        console.log(response);
        this.router.navigate(['/theses']);
      },
      error => {
        console.log(error);
      }
    )
  }

}
