import { TheseFromWebsite } from "./theseFromWebsite.model";

export class Response {
    docs?: TheseFromWebsite;
    numFound?: number;
}