import { FacetCounts } from "./facet-counts.model";
import { Response } from "./response.model";
import { ResponseHeader } from "./responseHeader.model";
import { TheseFromWebsite } from "./theseFromWebsite.model";

//création du model TheseFromWebsite
export class ListTheseFromWebsite {
    facet_counts?: FacetCounts;
    response?: TheseFromWebsite;
    responseHeader?: ResponseHeader;
}

