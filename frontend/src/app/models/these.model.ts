//création du model These
/*
export class These {
        id?: any;
        title?: string;
        description?: string;
        date?: string;            
}
*/
 export class These { 
        id ?: any;
        href?: string;
        dateInsert?: Date;
        dateMaj?: Date;
        status?: string;
        accessible?: string;
        titre?: string;
        auteurPpn?: number;
        auteur?: string;
        etabSoutenance?: string;
        etabSoutenancePpn?: number; 
        dateSoutenance?: Date; 
        discipline?: string ;
        num?: String; 
        langueThese?: []; 
        personne?: [];
        ppn?: [];
        oaiSetSpec?: [];
        directeurThesePpn?: [];
        directeurTheseNP?: []; 
        directeurThese?: [];
        etablissement?: [];
 }