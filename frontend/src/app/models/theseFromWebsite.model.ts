//création du model TheseFromWebsite
export class TheseFromWebsite { 
        id ?: number;
        dateInsert?: Date;
        dateMaj?: Date;
        status?: string;
        accessible?: boolean;
        titre?: string;
        auteurPpn?: number;
        href?: string;
        auteur?: string;
        etabSoutenance?: string;
        etabSoutenancePpn?: number; 
        dateSoutenance?: Date; 
        discipline?: string ;
        num?: String; 
        langueThese?: []; 
        personne?: [];
        ppn?: [];
        oaiSetSpec?: [];
        directeurThesePpn?: [];
        directeurTheseNP?: []; 
        directeurThese?: [];
        etablissement?: []; 

}