import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { These } from '../models/these.model';

//on définit baseUrl avec l'api du back
const baseUrl= "http://localhost:8080/api/theses"


@Injectable({
  providedIn: 'root'
})


export class TheseService {

  //on fait appel à httpClient pour appeler les trucs du back
  constructor(private http:HttpClient) { }

  //on définit une fonction getAll qui selectionne toutes les theses (ça fait appel à la fonction définit dans le back, les suivantes feront la même chose)
  getAll(): Observable<These[]> {
    return this.http.get<These[]>(baseUrl);
  }

  //on définit une fonction qui récupère une seule these
  get(id: any): Observable<These> {
    return this.http.get(`${baseUrl}/${id}`);
  }

  //fonction qui permet d'enregistrer une these
  create(data: any): Observable<any> {
    return this.http.post(baseUrl, data);
  }

  //fonction qui permet de modifier une these
  update(id:any, data:any): Observable<any> {
    return this.http.put(`${baseUrl}/${id}`, data);
  }

  //fonction qui permet de supprimer une thèse grâce à son id.
  delete(id: any): Observable<any> {
    return this.http.delete(`${baseUrl}/${id}`);
  }

  //fonction qui permet de supprimer toutes les thèses
  deleteAll(): Observable<any> {
    return this.http.delete(baseUrl);
  }

  //trouver grâce au titsre
  findByTitle(title: any): Observable<These[]> {
    return this.http.get<These[]>(`${baseUrl}?title=${title}`);
  }



}
