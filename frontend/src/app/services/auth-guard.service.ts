import { Injectable } from '@angular/core';
import { CanLoad, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { filter, map, take } from 'rxjs/operators';


/**
 * Le service Auth Guard permet de vérifier que l'utilisateur est connecté, si oui, on est redirigé vers Tab1Page, sinon vers SigninPage
 */
@Injectable()
export class AuthGuardService implements CanLoad {
  
  /**
   * Constructeur de la classe
   * 
   * @param router {Router} Injection du router pour la redirection
   * @param authService {AuthService} Injection du service d'authentification
   */
  constructor(private router: Router, private authService: AuthService) { }
  
  /**
   * Fonction permettant de retourner true si l'utilisateur est connecté
   * 
   * @returns Promise contenant un boléen
   */
  canLoad(): Observable<boolean> {   
    return this.authService.isAuthenticated.pipe(
      filter(val => val !== null), // Filter out initial Behaviour subject value
      take(1), // Otherwise the Observable doesn't complete!
      map(isAuthenticated => {
        if (isAuthenticated) {    
          return true;
        } else {         
          this.router.navigate(['/tabs', 'tab3']);
          return false;
        }
      })
    );
  }
}
