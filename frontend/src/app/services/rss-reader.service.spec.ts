import { TestBed } from '@angular/core/testing';

import { RssReaderService } from './rss-reader.service';

describe('RssReaderService', () => {
  let service: RssReaderService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RssReaderService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
