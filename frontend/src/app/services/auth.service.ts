import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { TokenStorageService } from './token-storage.service';

const AUTH_API = 'http://localhost:8080/api/auth';
const  httpOptions = {
  headers: new HttpHeaders({'Content-Type':'application/json',
  'Access-Control-Allow-Origin':'*',
  'Access-Control-Allow-Headers': 'X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method',
  'Cache-Control': 'no-cache'
  })
};


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  
  userData: any;

  isAuthenticated: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor(private http: HttpClient, private tokenStorageService: TokenStorageService) { }


  //pour se connecter
  login(username: string, password:string): Observable<any> {
    return this.http.post(AUTH_API + '/signin',{
      username,
      password

    },httpOptions);
  }

  //pour s'enregistrer
  register(username: string, email: string, password:string):Observable<any>{
    return this.http.post(AUTH_API + '/signup', {
      username,
      email,
      password}, httpOptions);
    }
/*
    isAuthenticated() {
      if (this.tokenStorageService.getToken()) {
        return true;
      } else {
        return false;
      }
    } */
}
