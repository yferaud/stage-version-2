import { NgModule }                 from '@angular/core';
import { RouterModule, Routes }     from '@angular/router';
import { AddTheseComponent }        from './components/add-these/add-these.component';
import { TheseDetailsComponent }    from './components/these-details/these-details.component';
import { ThesesListComponent }      from './components/theses-list/theses-list.component';
import { RegisterComponent }        from './components/register/register.component';
import { LoginComponent }           from './components/login/login.component';
import { HomeComponent }            from './components/home/home.component';
import { ProfileComponent }         from './components/profile/profile.component';
import { BoardUserComponent }       from './components/board-user/board-user.component';
import { BoardModeratorComponent }  from './components/board-moderator/board-moderator.component';
import { BoardAdminComponent }      from './components/board-admin/board-admin.component';
import { RecuperationThesesSiteOfficielComponent } from './components/recuperation-theses-site-officiel/recuperation-theses-site-officiel.component';
import { TwitterComponent }         from './components/twitter/twitter.component';
import { RssReaderComponent } from './components/rss-reader/rss-reader.component';
//on définit les routes pour pouvoir se déplacer dans le site.
const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'theses'                              , component: ThesesListComponent  },
  { path: 'add'                                 , component: AddTheseComponent    },
  { path: 'theses/:id'                          , component: TheseDetailsComponent},
  { path: 'home'                                , component: HomeComponent },
  { path: 'login'                               , component: LoginComponent },
  { path: 'register'                            , component: RegisterComponent },
  { path: 'profile'                             , component: ProfileComponent },
  { path: 'user'                                , component: BoardUserComponent }    ,
  { path: 'mod'                                 , component: BoardModeratorComponent },
  { path: 'admin'                               , component: BoardAdminComponent }  ,
  { path: 'recuperationTheseDepuisSiteOfficiel' , component: RecuperationThesesSiteOfficielComponent},
  { path: 'twitter'                             , component: TwitterComponent},
  { path: 'rssReader'                           , component: RssReaderComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
