// import des elements
const express    = require('express');
const bodyParser = require("body-parser");
const cors       = require('cors');


//création de l'application express
const app=express();

//on définit avec quel port on peut envoyer et récupérer des données.
var corsOptions = { origin : "http://localhost:4200"};

//l'app express utilise le cors
app.use(cors(corsOptions));

// on parse le json - application/json
app.use(bodyParser.json());

//on parse urlenconded - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

//on se connecte à la base de données à l'aide de la method connect()
const db = require("./app/models");
const Role = db.role;

db.mongoose
    .connect(db.url,
    {
      useNewUrlParser: true,
      useUnifiedTopology: true
    })
    //si la connexion est réussie, on console.log "connected to the database"
    .then(() => {
      console.log("Connected to the database!");
      initial();
    })
    //si la connexion échoue, on le dit également dans la console et on termine le process.
    .catch(err => {
      console.log("Cannot connect to the database!", err);
      process.exit();
    });

// simple route pour tester
app.get("/", (req, res) => {
    res.json({ message: "Bienvenue sur l'application." });
});


//on fait appel aux routes définies dans le fichier these.routes.js du dossier routes
require("./app/routes/these.routes")(app);
//on fait appel aux routes définies dans le fichier auth.routes.js du dossier routes
require("./app/routes/auth.routes")(app);
//on fait appel aux routes définies dans le fichier user.routes.js
require("./app/routes/user.routes")(app);
//on fait appel à la route du lecteur rss
//require('./app/routes/rssReader.routes')
// definir port 8080 comme port du server, listen for request
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});


// la fonction initial() nous aide à créer 3 lignes importantes dans la collection roles.
function initial() {
  Role.estimatedDocumentCount((err, count) =>{
    // s'il n'y a pas d'erreur et que le nombre de role est strictement égale à 0
    if (!err && count ===0) {
      //création d'un rôle user
      new Role({
        name: "user"
      }).save(err => {
        if (err) {
          console.log("error", err );
        }

        console.log("added 'user' to roles collection");
      });


      //création d'un rôle "moderator"
      new Role({
        name:"moderator"
      }).save(err => {
        if (err) {
          console.log("error", err);
        }

        console.log("added 'moderator' to roles collection");
      });

      //création d'un rôle admin
      new Role({
        name:"admin"
      }).save(err=>{
        if (err) {
          console.log("error", err);
        }

        console.log("added 'admin' to roles collection");
      });

    }
  });
}


/*
const { parse } = require('rss-to-json');
*/
/*
//on transforme le xml en json
(async () => {

  var rss = await parse('https://www.ssi.gouv.fr/feed/actualite/');

  console.log(JSON.stringify(rss, null, 3));

})();
*/

