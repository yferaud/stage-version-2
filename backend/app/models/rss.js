const mongoose = require ("mongoose");
//création du model d'un rôle qui consiste 
const Rss = mongoose.model(
    "Rss",
    new mongoose.Schema({
        title: String,
        description:'',
        link: String,
        image: "",
        category: [],
        items: [
            {
                type:mongoose.Schema.Types.ObjectId,
                ref: "item"
            }
        ]
    })
);

module.exports= rss;