module.exports = mongoose => {
   /* var schema = mongoose.Schema(
      {
        title: String,
        description: String,
        date: String
      },
   // normalement pas nécessaire, je le commente au cas où j'en ai besoin pour une raison x ou y   { timestamps: true }
    );
  */
  var schema =mongoose.Schema(
    {
      
        dateInsert: Date,
        dateMaj: Date,
        href: String,
        status: String,
        accessible: String,
        titre: String,
        auteurPpn: Number,
        auteur: String,
        etabSoutenance: String,
        etabSoutenancePpn: Number, 
        dateSoutenance: Date,
        discipline: String,
        num: String,
        langueThese: [], 
        personne: [],
        ppn: [],
        oaiSetSpec: [],
        directeurThesePpn: [],
        directeurTheseNP: [],
        directeurThese: [],
        etablissement: []
    }
  )
 



    /* cette method permettra de transformer le _id de mongodb (donc l'objectId)
     en id normal pour qu'il soit utilisable depuis la partie frontend */
    schema.method("toJSON", function() {
      const { __v, _id, ...object } = this.toObject();
      object.id = _id;
      return object;
    });
  
    //These utilise donc le model défini et la method pour avoir un id au lieu d'un objectId .
    const These = mongoose.model("these", schema);
    return These;
  };
  