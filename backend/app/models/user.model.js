const mongoose = require("mongoose");

//modèle d'utilisateur, avec un nom, un email, un mot de passe, et un rôle, dont on a créé le modèle précédement
const User = mongoose.model(
    "User",
    new mongoose.Schema({
        username: String,
        email: String,
        password: String,
        roles: [
            {
                type:mongoose.Schema.Types.ObjectId,
                ref:"Role"
            }
        ]
    })
);

module.exports = User;