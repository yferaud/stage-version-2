//appelle de la base de données définie précédement 
const dbConfig = require('../../config/db.config');

//appelle de mongoose
const mongoose = require("mongoose");
//permet les opérations asynchrones
mongoose.Promise = global.Promise;


//création d'un objet db? auquel on attribue les différents paramètres
const db = {};

db.mongoose = mongoose;
//db récupère le modèle user
db.user = require("./user.model");
//db récupère le modèle role
db.role = require("./role.model");

db.ROLES = ["user", "admin", "moderator"];

//db récupère l'url de la base de données défini dans la config.
db.url = dbConfig.url;
//récupérations du model pour les thèses.
db.theses = require("./these.model.js")(mongoose);


//export pour pouvoir l'appeler dans les autres fichiers.
module.exports = db;
