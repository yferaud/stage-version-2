//indique qu'il s'agit de contenu qui ne nécessite aucun rôle aprticulier et est ouvert à tout le monde.
exports.allAccess = (req, res) => {
  res.status(200).send("Public Content.");
};


//indique qu'il s'agit de contenu qui nécessite le rôle d'utilisateur'
exports.userBoard = (req, res) => {
  res.status(200).send("User Content.");
};

//indique qu'il s'agit de contenu qui nécessite le rôle d' admin
exports.adminBoard = (req, res) => {
  res.status(200).send("Admin Content.");
};

//indique qu'il s'agit de contenu qui nécessite le rôle de moderateur 
exports.moderatorBoard = (req, res) => {
  res.status(200).send("Moderator Content.");
};