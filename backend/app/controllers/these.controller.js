const db = require("../models");
const These = db.theses;

// Créer et sauvegarder une nouvelle thèse
exports.create = (req, res) => {
    // Valider la requête
    if (!req.body.titre) {
      res.status(400).send({ message: "Le contenu ne peut pas être vide!" });
      return;
    }
  
    // Créer These
    const these = new These({
      titre: req.body.titre,
      auteur: req.body.auteur,
      dateInsert: req.body.dateInsert,
      dateMaj: req.body.dateMaj,
      status: req.body.status,
      accessible: req.body.accessible,
      titre: req.body.titre,
      auteurPpn: req.body.auteurPpn,
      href:req.body.href,
      auteur: req.body.auteur,
      etabSoutenance: req.body.etabSoutenance,
      etabSoutenancePpn: req.body.etabSoutenancePpn, 
      dateSoutenance: req.body.dateSoutenance , 
      discipline:req.body.discipline,
      num: req.body.num, 
      langueThese: req.body.langueThese, 
      personne: req.body.personne,
      ppn: req.body.ppn,
      oaiSetSpec: req.body.oaiSetSpec,
      directeurThesePpn: req.body.directeurThesePpn,
      directeurTheseNP: req.body.directeurTheseNP,
      directeurThese: req.body.directeurThese,
      etablissement: req.body.etablissement,
    });
  
    // Sauvegarder These dans la base de données
    these
      .save(these)
      .then(data => {
        res.send(data);
      })
      //erreur
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Une erreur est survenue durant la création de la these."
        });
      });
};




//Trouver toutes les thèses de la bdd.
exports.findAll = (req, res) => {
    const titre = req.query.titre;
    var condition = titre ? { titre: { $regex: new RegExp(titre), $options: "i" } } : {};
  
    These.find(condition)
      //réussie
      .then(data => {
        res.send(data);
      })
      //erreur
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Une erreur est survenue durant la tentative de récupération des données."
        });
      });
  };

//Trouver une thèse en particulier dans la bdd.
exports.findOne = (req, res) => {
    const id = req.params.id;
  
    These.findById(id)
      //réussi
      .then(data => {
        //pas de donnée trouvée
        if (!data)
          res.status(404).send({ message: "Nous n'avons rien trouvé avec cet id: " + id });
        else res.send(data);
      })
      //echec
      .catch(err => {
        res
          .status(500)
          .send({ message: "Une erreur est survenue pendant la tentative de récupération de thèses à l'aide de l'id:" + id });
      });
  };

// modifier une thèse par son id dans la requête
exports.update = (req, res) => {
    //si le champs est vide
    if (!req.body) {
      return res.status(400).send({
        message: "Les données à modifier ne peuvent être vides."
      });
    }
  
    const id = req.params.id;
  
    These.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
      //succès
      .then(data => {
        //s'il n'y a pas de données 
        if (!data) {
          res.status(404).send({
            message: `Impossible  de modifier la These avec l'id=${id}. Peut-être que la thèse n'as pas été trouvée!`
          });
        } else res.send({ message: "La thèse a été modifiée avec succès." });
      })
      //echec
      .catch(err => {
        res.status(500).send({
          message: "Une erreur est survenu durant la modification de la thèse avec l'id =" + id
        });
      });
  };
//Supprimer une thèse en particulier grace à son id dans la requête.
exports.delete = (req, res) => {
    const id = req.params.id;
  
    These.findByIdAndRemove(id)
      //succès
      .then(data => {
         //s'il n'y a pas de données 
        if (!data) {
          res.status(404).send({
            message: `Ne peut pas supprimer la These avec l'Id=${id}. Peut-être que la thèse n'a pas été trouvée!`
          });
        } else {
          res.send({
            message: "La These a été supprimée avec succès!"
          });
        }
      })
      //si echec
      .catch(err => {
        res.status(500).send({
          message: "Ne peut pas supprimer These avec l'id=" + id
        });
      });
  };

//Supprimer toutes les thèses de la base de données.
exports.deleteAll = (req, res) => {
    These.deleteMany({})
      //si réussite
      .then(data => {
        res.send({
          message: `${data.deletedCount} Theses ont été supprimées avec succès.`
        });
      })
      //si echec
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Une erreur est survenu durant la suppression des thèses."
        });
      });
  };
