const jwt    = require ("jsonwebtoken");
const config = require ("../../config/auth.config.js");
const db     = require ("../models");
const User   = db.user;
const Role   = db.role;


// vérifie que le token pour la connexion est bien correct
verifyToken  = (req, res, next) => {
    let token = req.headers["x-access-token"];

    //s'il n'y a pas de token
    if (!token) {
        return res.status(403).send({ message: "Pas de token fourni."});
    }
    //si le token ne correspond pas 
    jwt.verify(token, config.secret, (err, decoded) => {
        if (err) {
            return res.status(401).send({message: "Unauthorized!"});
        }
        req.userId = decoded.id;
        next();
    });
};

//verification de la possession du rôle admin.
isAdmin = (req, res, next) => {
    User.findById(req.userId).exec((err, user) =>{
        if (err) {
            res.status(500).send({ message: err});
            return;
        }

        Role.find(
            {
                _id: {$in: user.roles}
            },
            (err, roles) => {
                if (err) {
                    res.status(500).send({ message: err});
                    return;
                }

                for (let i=0; i< roles.length; i++) {
                    if (roles[i].name === "admin") {
                        next();
                        return;
                    }
                }

                res.status(403).send({message: "Le role admin est requis"});
                return;
            }
        );
    });
};



//verification de la possession du rôle moderateur
isModerator = (req, res, next) => {
  User.findById(req.userId).exec((err, user) => {
    if (err) {
      res.status(500).send({ message: err });
      return;
    }

    Role.find(
      {
        _id: { $in: user.roles }
      },
      (err, roles) => {
        if (err) {
          res.status(500).send({ message: err });
          return;
        }

        for (let i = 0; i < roles.length; i++) {
          if (roles[i].name === "moderator") {
            next();
            return;
          }
        }

        res.status(403).send({ message: "Require Moderator Role!" });
        return;
      }
    );
  });
};


//utilisation de ce qui est créé dans cette page.
const authJwt = {
  verifyToken,
  isAdmin,
  isModerator
};
module.exports = authJwt;