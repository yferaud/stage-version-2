const db    = require ("../models");
const ROLES = db.ROLES;
const User  = db.user;

//verifie que le  nom de l'utilisateur et/ou le mail sont disponible.
checkDuplicateUsernameOrEmail = (req, res, next) => {
    //Username
    User.findOne({
        username:req.body.username
    }).exec((err, user) => {
        //renvoie un message d'erreur s'il y a une erreur dans le prrocessus
        if (err) {
            res.status(500).send({message:err});
            return;
        }
        //s'il y a déjà un utilisateur à ce nom, renvoie le message d'erreur.
        if (user) {
            res.status(400).send({ message: "Erreur, le nom d'utilisateur est déjà utilisé."});
            return;
        }

        //Email
        User.findOne({
            email: req.body.email
        }).exec((err, user) => {
            if (err) {
                res.status(500).send({message:err});
                return;
            }
            //renvoie un message d'erreur si un utilisateur utilise déjà cette email;
            if (user) {
                res.status(400).send({ message:"Erreur, cet Email est déjà utilisé!"});
                return;
            }

            next();
            });
    });  
};

//verifie l'existence des roles.
checkRolesExisted = (req, res, next) => {
    if (req.body.roles) {
        for (let i = 0; i < req.body.roles.length; i++) {
            if (!ROLES.includes(req.body.roles[i])) {
                res.status(400).send({
                    message: 'Failed! Role ${req.body.roles[i]} does not exist!'
                });
                return;
            }
        }
    }

    next();
};


//utilisation des deux trucs créés au dessus
const verifySignUp = {
    checkDuplicateUsernameOrEmail,
    checkRolesExisted
};

module.exports =verifySignUp;