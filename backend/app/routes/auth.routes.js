//import des elements nécessaires
const { verifySignUp } = require("../middlewares");
const controller = require("../controllers/auth.controller");

module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });


  //création d'une route pour accéder à la fonction signup
  app.post(
    "/api/auth/signup",
    [
      //verifie que les noms d'utilisateurs, les emails soient bien uniques
      verifySignUp.checkDuplicateUsernameOrEmail,
      //verification que le role soit bien existant.
      verifySignUp.checkRolesExisted
    ],
    controller.signup
  );
  //création d'une route pour le signin
  app.post("/api/auth/signin", controller.signin);
};
