const { authJwt } = require("../middlewares");
const controller = require("../controllers/user.controller");

module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });
  //creation d'une route vers le contenu non restreint
  app.get("/api/test/all", controller.allAccess);

  //creation d'une route vers le contenu restreint aux user
  app.get("/api/test/user", [authJwt.verifyToken], controller.userBoard);

 //route vers le contenu réservé aux moderateurss (verification de la connexion et du fait que le rôle moderator soit bien attribué)
  app.get(
    "/api/test/mod",
    [authJwt.verifyToken, authJwt.isModerator],
    controller.moderatorBoard
  );

 //route vers le contenu réservé aux admins (verification de la connexion et du fait que le rôle admins soit bien attribué)
  app.get(
    "/api/test/admin",
    [authJwt.verifyToken, authJwt.isAdmin],
    controller.adminBoard
  );
};
