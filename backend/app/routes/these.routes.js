module.exports = app => {
    const theses = require("../controllers/these.controller.js");
  
    var router = require("express").Router();
  
    // Create a new These
    router.post("/", theses.create);
  
    // Retrieve all theses
    router.get("/", theses.findAll);
  
  
    // Retrieve a single These with id
    router.get("/:id", theses.findOne);
  
    // Update a These with id
    router.put("/:id", theses.update);
  
    // Delete a These with id
    router.delete("/:id", theses.delete);
  
    // supprimer tous les a new These
    router.delete("/", theses.deleteAll);
    

    app.use('/api/theses', router);
};