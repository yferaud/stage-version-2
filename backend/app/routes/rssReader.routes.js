module.exports = app => {
    const rssReader = require("../controllers/rssReader.controller.js");
  
    var router = require("express").Router();

    router.get("/", rssReader.findAll);

    app.use('/api/rss', router);

}